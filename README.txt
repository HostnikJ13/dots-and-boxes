﻿Igrica pike in kvadrati oz. Dots and Boxes poteka na pravokotni plošči. 
Igralca izmenično povezujeta po dve točki ali vodoravno ali navpično. 
Ko igralec nariše zadnjo stranico kvadratka, ta postane njegov in ima nato še eno potezo. 
Zmaga tisti, ki ima na koncu v lasti več kvadratkov.