from tkinter import *
from random import *

a = 50  # razdalja med pikami


def drugi_igralec(igralec):
    """Izračuna nasprotnega igralca."""
    if igralec == 'Igralec 1':
        return 'Igralec 2'
    else:
        return 'Igralec 1'


class Polje():
    def __init__(self, master, m=14, n=7):
        """Števili m in n določata število kvadratkov(dolžino in širino)."""
        # Izračunamo dolžino in širino polja(k m in n prištejemo 1,
        # da bo okoli igralnega polja še rob širine a/2).
        self.m = m
        self.n = n
        self.dolzina = (m+1)*a
        self.sirina = (n+1)*a

        # Nastavimo, da bo prvi na potezi Igralec 1.
        self.na_potezi = StringVar(master, value='Igralec 1')

        # Ustvarimo platno.
        self.polje = Canvas(master, width=self.dolzina, height=self.sirina)
        self.polje.grid(row=3, column=0, columnspan=100)
        self.polje.bind('<Button-1>', self.klik)

        # Glavni menu
        menu = Menu(master)
        master.config(menu=menu)
        igra_menu = Menu(menu)
        menu.add_cascade(label="Nova igra", menu=igra_menu)
        igra_menu.add_command(label="Človek - Človek", command=lambda: self.nova_igra(self.m, self.n, 'človek', 'človek'))
        igra_menu.add_command(label="Človek - Računalnik", command=lambda: self.nova_igra(self.m, self.n, 'človek', 'računalnik'))
        igra_menu.add_command(label="Računalnik - Človek", command=lambda: self.nova_igra(self.m, self.n, 'računalnik', 'človek'))
        igra_menu.add_command(label="Računalnik - Računalnik", command=lambda: self.nova_igra(self.m, self.n, 'računalnik', 'računalnik'))
        menu.add_command(label="Izhod", command=master.destroy)

        # Napis, ki kaže, kateri igralec je na potezi.
        Label(master, text="Na potezi je: ").grid(row=0, column=0)
        Label(master, textvariable=self.na_potezi).grid(row=0, column=1)

        # Napis, ki se prikaže ob koncu igre.
        self.konec_var = StringVar(master, value="\n")
        Label(master, textvariable=self.konec_var).grid(row=4, column=0, columnspan=100)

        self.igralec_1 = 'človek'
        self.igralec_2 = 'človek'

        self.nova_igra(self.m, self.n, 'človek', 'človek')

    def nova_igra(self, m, n, igralec_1, igralec_2):
        self.igralec_1 = igralec_1
        self.igralec_2 = igralec_2
        self.konec_var.set("\n")
        self.na_potezi.set('Igralec 1')
        self.polje.delete(ALL)

        # Naredimomo seznam s koordinatami pik.
        # Prištetih a/2 pomeni zamik, da okoli pik nastane rob.
        self.pike = [(j*a+a/2, i*a+a/2) for i in range(n+1) for j in range(m+1)]

        # Narišemo pike s premerom a/7.
        for (i, j) in self.pike:
            self.polje.create_oval(i-a/14, j-a/14, i+a/14, j+a/14, fill='#000000')

        # Seznama, v katera bomo dodajali že narisane črte.
        # Vsebujeta že pike po desnem in po spodnjem robu, saj iz njih ne rišemo črt.
        self.navpicne = [(j*a+a/2, self.n*a+a/2) for j in range(m+1)]
        self.vodoravne = [(self.m*a+a/2, i*a+a/2) for i in range(n+1)]

        # Dosežene točke obeh igralcev.
        self.tocke_1 = 0
        self.tocke_2 = 0
        self.var_1 = StringVar(root, value=self.tocke_1)
        self.var_2 = StringVar(root, value=self.tocke_2)
        Label(root, text="Igralec 1: ", foreground='#FF0000').grid(row=1, column=0)
        Label(root, textvariable=self.var_1).grid(row=1, column=1)
        Label(root, text="točk").grid(row=1, column=2)
        Label(root, text="Igralec 2: ", foreground='#0000FF').grid(row=2, column=0)
        Label(root, textvariable=self.var_2).grid(row=2, column=1)
        Label(root, text="točk").grid(row=2, column=2)

        if self.igralec_1 == 'računalnik':
            self.racunalnik()

    def vrednost(self):
        """Metoda, ki oceni vrednost igre glede na točke obeh igralcev."""
        igralec = self.na_potezi.get()
        nasprotnik = drugi_igralec(igralec)
        if igralec == 'Igralec 1':
            t1 = self.tocke_1
            t2 = self.tocke_2
        else:
            t1 = self.tocke_2
            t2 = self.tocke_1
        max_igralec = max(t1-t2, 0)
        max_nasprotnik = max(t2-t1, 0)
        if max_igralec >= max_nasprotnik:
            return max_igralec
        else:
            return -max_nasprotnik

    def racunalnik(self):
        """Igranje računalnika:
           najprej odigra poteze, ki mu prinašajo točke,
           potem odigra potezo, ki ne prinaša točk nasprotniku,
           če pa to ni možno, poišče najboljšo možno potezo."""

        def najdi_kvadratek():
            """Poišče poteze, ki prinašajo točke."""
            mozne_navpicne = [pika for pika in self.pike if pika not in self.navpicne]
            mozne_vodoravne = [pika for pika in self.pike if pika not in self.vodoravne]
            for (x,y) in mozne_navpicne:
                self.navpicne.append((x,y))
                if self.je_kvadratek((x-a/2)/a, (y-a/2)/a):
                    self.navpicne.pop()
                    return ((x-a/2)/a, (y-a/2)/a, 'leva')
                elif self.je_kvadratek((x-a/2)/a-1, (y-a/2)/a):
                    self.navpicne.pop()
                    return ((x-a/2)/a-1, (y-a/2)/a, 'desna')
                else:
                    self.navpicne.pop()
            for (x,y) in mozne_vodoravne:
                self.vodoravne.append((x,y))
                if self.je_kvadratek((x-a/2)/a, (y-a/2)/a):
                    self.vodoravne.pop()
                    return ((x-a/2)/a, (y-a/2)/a, 'zgornja')
                elif self.je_kvadratek((x-a/2)/a, (y-a/2)/a-1):
                    self.vodoravne.pop()
                    return ((x-a/2)/a, (y-a/2)/a-1, 'spodnja')
                else:
                    self.vodoravne.pop()
        
        def sta_ze_2(x, y, kam_igram):
            """Preprečimo igranje poteze, ki bi nasprotniku omogočila osvojitev točk."""
            if kam_igram == 'igraj_navpično':
                if x != self.m*a+a/2:
                    return (((x-a,y) in self.vodoravne and (x-a,y+a) in self.vodoravne) or
                            ((x-a,y) in self.vodoravne and (x-a,y) in self.navpicne) or
                            ((x-a,y+a) in self.vodoravne and (x-a,y) in self.navpicne) or
                            ((x,y) in self.vodoravne and (x,y+a) in self.vodoravne) or
                            ((x,y) in self.vodoravne and (x+a,y) in self.navpicne) or
                            ((x,y+a) in self.vodoravne and (x+a,y) in self.navpicne))
                else:
                    return (((x-a,y) in self.vodoravne and (x-a,y+a) in self.vodoravne) or
                            ((x-a,y) in self.vodoravne and (x-a,y) in self.navpicne) or
                            ((x-a,y+a) in self.vodoravne and (x-a,y) in self.navpicne))
            else:
                if y != self.n*a+a/2:
                    return (((x,y-a) in self.navpicne and (x+a,y-a) in self.navpicne) or
                            ((x,y-a) in self.vodoravne and (x,y-a) in self.navpicne) or
                            ((x,y-a) in self.vodoravne and (x+a,y-a) in self.navpicne) or
                            ((x,y) in self.navpicne and (x+a,y) in self.navpicne) or
                            ((x,y) in self.navpicne and (x,y+a) in self.vodoravne) or
                            ((x,y+a) in self.vodoravne and (x+a,y) in self.navpicne))
                else:
                    return (((x,y-a) in self.navpicne and (x+a,y-a) in self.navpicne) or
                            ((x,y-a) in self.vodoravne and (x,y-a) in self.navpicne) or
                            ((x,y-a) in self.vodoravne and (x+a,y-a) in self.navpicne))

        def alphabeta(globina, alpha, beta, igralec):
            """Ne dela ..."""
            najvec = 2*(self.n+1)*(self.m+1)
            najmanj = -najvec
            mozne_navpicne = [pika for pika in self.pike if pika not in self.navpicne]
            mozne_vodoravne = [pika for pika in self.pike if pika not in self.vodoravne]
            mozne_poteze = []
            for navpicna in mozne_navpicne:
                mozne_poteze.append((navpicna,'leva'))
            for vodoravna in mozne_vodoravne:
                mozne_poteze.append((vodoravna,'zgornja'))
            
            if globina == 0 or self.konec(self.tocke_1, self.tocke_2):
                vrednost = self.vrednost()
                if not igralec:
                    vrednost = -vrednost
                return (None, vrednost)
            else:
                if igralec:
                    p = None
                    vrednost_p = najmanj
                    for poteza in mozne_poteze:
                        if poteza[1] == 'leva':
                            self.navpicne.append(poteza[0])
                            (q, vrednost_q) = alphabeta(globina-1, alpha, beta, False)
                            self.navpicne.pop()
                        else:
                            self.vodoravne.append(poteza[0])
                            (q, vrednost_q) = alphabeta(globina-1, alpha, beta, False)
                            self.vodoravne.pop()
                        if vrednost_q > vrednost_p:
                            p = poteza
                            vrednost_p = vrednost_q
                        alpha = max(alpha, vrednost_p)
                        if beta <= alpha:
                            return (p, vrednost_p)
                    return (p, vrednost_p)
                else:
                    p = None
                    vrednost_p = najvec
                    for poteza in mozne_poteze:
                        if poteza[1] == 'leva':
                            self.navpicne.append(poteza[0])
                            (q, vrednost_q) = alphabeta(globina-1, alpha, beta, True)
                            self.navpicne.pop()
                        else:
                            self.vodoravne.append(poteza[0])
                            (q, vrednost_q) = alphabeta(globina-1, alpha, beta, True)
                            self.vodoravne.pop()
                        if vrednost_q < vrednost_p:
                            p = poteza
                            vrednost_p = vrednost_q
                        beta = min(beta, vrednost_p)
                        if beta <= alpha:
                            return (p, vrednost_p)
                    return (p, vrednost_p)

        def minimax(globina):
            """Dela neučinkovito ..."""
            mozne_navpicne = [pika for pika in self.pike if pika not in self.navpicne]
            mozne_vodoravne = [pika for pika in self.pike if pika not in self.vodoravne]
            mozne_poteze = []
            for navpicna in mozne_navpicne:
                mozne_poteze.append((navpicna,'leva'))
            for vodoravna in mozne_vodoravne:
                mozne_poteze.append((vodoravna,'zgornja'))
                
            if globina == 0 or self.konec(self.tocke_1, self.tocke_2):
                return (None, self.vrednost())
            else:
                p = None
                vrednost_p = None
                for poteza in mozne_poteze:
                    if poteza[1] == 'leva':
                        self.navpicne.append(poteza[0])
                        (q, vrednost_q) = minimax(globina - 1)
                        self.navpicne.pop()
                    else:
                        self.vodoravne.append(poteza[0])
                        (q, vrednost_q) = minimax(globina - 1)
                        self.vodoravne.pop()
                    vrednost_q = -vrednost_q
                    if (vrednost_p is None) or vrednost_q > vrednost_p:
                        p = poteza
                        vrednost_p = vrednost_q
                return (p, vrednost_p)
        
        if najdi_kvadratek() != None:
            self.poteza(najdi_kvadratek()[0], najdi_kvadratek()[1], najdi_kvadratek()[2])
        else:
            if self.na_potezi.get() == 'Igralec 1':
                igralec = True
            else:
                igralec = False
            mozne_poteze = []
            if len(self.navpicne) < (self.m+1)*(self.n+1):
                mozne_poteze.append('igraj_navpično')
            if len(self.vodoravne) < (self.m+1)*(self.n+1):
                mozne_poteze.append('igraj_vodoravno')
            kam_igram = choice(mozne_poteze)
            if kam_igram == 'igraj_navpično':
                stranica = 'leva'
                dobre_poteze = [pika for pika in self.pike if (pika not in self.navpicne and not sta_ze_2(pika[0], pika[1], kam_igram))]
                if dobre_poteze != []:
                    (x,y) = choice(dobre_poteze)
                else:
                    (x,y) = choice([pika for pika in self.pike if pika not in self.navpicne])
                    #((x,y), stranica) = minimax(5)[0]
                    #((x,y), stranica) = alphabeta(100, 2*(self.n+1)*(self.m+1), (-2)*(self.n+1)*(self.m+1), igralec)[0] 
            elif kam_igram == 'igraj_vodoravno':
                stranica = 'zgornja'
                dobre_poteze = [pika for pika in self.pike if (pika not in self.vodoravne and not sta_ze_2(pika[0], pika[1], kam_igram))]
                if dobre_poteze != []:
                    (x,y) = choice(dobre_poteze)
                else:
                    (x,y) = choice([pika for pika in self.pike if pika not in self.vodoravne])
                    #((x,y), stranica) = minimax(5)[0]
                    #((x,y), stranica) = alphabeta(100, 2*(self.n+1)*(self.m+1), (-2)*(self.n+1)*(self.m+1), igralec)[0]
            self.poteza((x-a/2)/a, (y-a/2)/a, stranica)

    def klik(self, event):
        """Metoda ob kliku."""
        i = (event.x - a/2) // a
        j = (event.y - a/2) // a
        stranica = self.najblizja(event.x, event.y)
        self.poteza(i, j, stranica)

    def poteza(self, i, j, stranica):
        """Glavna metoda."""
        # Ugotovimo, kdo je na potezi.
        igralec = self.na_potezi.get()

        # Če se je klik zgodil v igralnem polju:
        # - povežemo ustrezni piki,
        # - če je igralec povezal zadnjo stranico kvadratka, le tega narišemo in igralcu dodelimo ustrezno število točk
        # - poskrbimo za to, kdo je na potezi.
        if stranica == 'leva':
            if self.se_da_navpicno(a*i+a/2, a*j+a/2):
                self.narisi_navpicno(a*i+a/2, a*j+a/2)
                if self.je_kvadratek(i, j) and self.je_kvadratek(i-1, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.narisi_kvadratek(i-1, j, igralec)
                    self.podeli_tocke(igralec, 2)
                elif self.je_kvadratek(i, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.podeli_tocke(igralec, 1)
                elif self.je_kvadratek(i-1, j):
                    self.narisi_kvadratek(i-1, j, igralec)
                    self.podeli_tocke(igralec, 1)
                else:
                    self.na_potezi.set(drugi_igralec(igralec))
        elif stranica == 'desna':
            if self.se_da_navpicno(a*(i+1)+a/2, a*j+a/2):
                self.narisi_navpicno(a*(i+1)+a/2, a*j+a/2)
                if self.je_kvadratek(i, j) and self.je_kvadratek(i+1, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.narisi_kvadratek(i+1, j, igralec)
                    self.podeli_tocke(igralec, 2)
                elif self.je_kvadratek(i, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.podeli_tocke(igralec, 1)
                elif self.je_kvadratek(i+1, j):
                    self.narisi_kvadratek(i+1, j, igralec)
                    self.podeli_tocke(igralec, 1)
                else:
                    self.na_potezi.set(drugi_igralec(igralec))
        elif stranica == 'zgornja':
            if self.se_da_vodoravno(a*i+a/2, a*j+a/2):
                self.narisi_vodoravno(a*i+a/2, a*j+a/2)
                if self.je_kvadratek(i, j) and self.je_kvadratek(i, j-1):
                    self.narisi_kvadratek(i, j, igralec)
                    self.narisi_kvadratek(i, j-1, igralec)
                    self.podeli_tocke(igralec, 2)
                elif self.je_kvadratek(i, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.podeli_tocke(igralec, 1)
                elif self.je_kvadratek(i, j-1):
                    self.narisi_kvadratek(i, j-1, igralec)
                    self.podeli_tocke(igralec, 1)
                else:
                    self.na_potezi.set(drugi_igralec(igralec))
        elif stranica == 'spodnja':
            if self.se_da_vodoravno(a*i+a/2, a*(j+1)+a/2):
                self.narisi_vodoravno(a*i+a/2, a*(j+1)+a/2)
                if self.je_kvadratek(i, j) and self.je_kvadratek(i, j+1):
                    self.narisi_kvadratek(i, j, igralec)
                    self.narisi_kvadratek(i, j+1, igralec)
                    self.podeli_tocke(igralec, 2)
                elif self.je_kvadratek(i, j):
                    self.narisi_kvadratek(i, j, igralec)
                    self.podeli_tocke(igralec, 1)
                elif self.je_kvadratek(i, j+1):
                    self.narisi_kvadratek(i, j+1, igralec)
                    self.podeli_tocke(igralec, 1)
                else:
                    self.na_potezi.set(drugi_igralec(igralec))
        
        if not self.konec(self.tocke_1, self.tocke_2):
            if ((self.na_potezi.get() == 'Igralec 1' and self.igralec_1 == 'računalnik') or (self.na_potezi.get() == 'Igralec 2' and self.igralec_2 == 'računalnik')):
                self.polje.after(100, self.racunalnik)

    def najblizja(self, x, y):
        """Metoda, ki prejme koordinati klika
           in vrne najblizjo stranico kvadratka, v katerega smo kliknili."""
        # Klik mora biti dovolj blizu(a/7) stranice kvadratka,
        # sicer se ne zgodi nič.
        if x >= a/2 and y >= a/2:
            leva = (x - a*((x-a/2)//a) - a/2, 'leva')
            desna = (a*((x-a/2)//a) + a + a/2 - x, 'desna')
            zgornja = (y - a*((y-a/2)//a) - a/2, 'zgornja')
            spodnja = (a*((y-a/2)//a) + a + a/2 - y, 'spodnja')
            najmanjsa = min(leva, desna, zgornja, spodnja)
            if najmanjsa[0] <= a/7:
                return najmanjsa[1]
        elif x >= a/2 and y < a/2:
            if a/2 - y <= a/7:
                return 'spodnja'
        elif x < a/2 and y >= a/2:
            if a/2 - x <= a/7:
                return 'desna'
        else:
            return None

    def se_da_navpicno(self, i, j):
        """Metoda, ki vrne True, če navpična črta iz pike (i,j) še ni narisana in False sicer."""
        if (i, j) not in self.navpicne:
            return True
        else:
            return False

    def narisi_navpicno(self, i, j):
        """Metoda, ki iz dane točke nariše navpično črto."""
        self.navpicne.append((i, j))
        self.polje.create_line(i, j, i, j+a, fill="#000000", width=3)

    def se_da_vodoravno(self, i, j):
        """Metoda, ki vrne True, če vodoravna črta iz pike (i,j) še ni narisana in False sicer."""
        if (i, j) not in self.vodoravne:
            return True
        else:
            return False

    def narisi_vodoravno(self, i, j):
        """Metoda, ki iz dane točke nariše vodoravno črto."""
        self.vodoravne.append((i, j))
        self.polje.create_line(i, j, i+a, j, fill="#000000", width=3)

    def je_kvadratek(self, i, j):
        """Metoda, ki preveja, ali je že nastal kvadratek."""
        if (a*i+a/2, a*j+a/2) in self.navpicne and (a*(i+1)+a/2, a*j+a/2) in self.navpicne and (a*i+a/2, a*j+a/2) in self.vodoravne and (a*i+a/2, a*(j+1)+a/2) in self.vodoravne:
            return True
        return False

    def narisi_kvadratek(self, i, j, igralec):
        """Metoda, ki ustrezno pobarva kvadratek."""
        if igralec == 'Igralec 1':
            self.polje.create_rectangle(a*i+a/2, a*j+a/2, a*(i+1)+a/2, a*(j+1)+a/2, fill='#FF0000', width=3)
        else:
            self.polje.create_rectangle(a*i+a/2, a*j+a/2, a*(i+1)+a/2, a*(j+1)+a/2, fill='#0000FF', width=3)

    def podeli_tocke(self, igralec, tocke):
        """Igralcu dodelimo točke in poskrbimo za njihov prikaz na platu."""
        if igralec == 'Igralec 1':
            self.tocke_1 += tocke
            self.var_1.set(self.tocke_1)
        else:
            self.tocke_2 += tocke
            self.var_2.set(self.tocke_2)

    def konec(self, tocke_1, tocke_2):
        """Metoda, ki preverja, ali je igra končana."""
        if tocke_1+tocke_2 == self.m*self.n:
            if tocke_1 == tocke_2:
                self.konec_var.set("Konec igre\nIzenačeno!")
            elif tocke_1 < tocke_2:
                self.konec_var.set("Konec igre\nZmagal je Igralec 2!")
            else:
                self.konec_var.set("Konec igre\nZmagal je Igralec 1!")
            return True
        else:
            return False

root = Tk()

root.title('Dots and Boxes')

aplikacija = Polje(root)

root.mainloop()
